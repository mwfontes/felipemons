const puppeteer = require("puppeteer");
const jsdom = require("jsdom");
const fs = require("fs");

const { JSDOM } = jsdom;

let loadedPokemonsUrls = [];
let outputData;
let puppeteerBrowser;

const fetchPage = (url) => {
  console.log("Fetching " + url);

  // Prevents loading same Pokemon again
  loadedPokemonsUrls.push(url);

  puppeteerBrowser.newPage().then((page) => {
    return page
      .goto(`https://www.pokemon.com${url}`)
      .then(() => {
        return page.content();
      })
      .then((response) => {
        const dom = new JSDOM(response);
        const doc = dom.window.document;

        // Select data
        const pokemonNameEl = doc.querySelector(".pokedex-pokemon-pagination-title > div");
        const pokemonName = pokemonNameEl.innerHTML.slice(1, pokemonNameEl.innerHTML.indexOf("<span") - 1).trim();
        const pokemonNumber = doc.querySelector(".pokedex-pokemon-pagination-title .pokemon-number").innerHTML.slice(2);

        // Abilities
        const abilityEls = doc.querySelectorAll(".pokemon-stats-info > ul > li");
        const abilityHP = abilityEls[0].querySelector("ul > li").dataset.value;
        const abilityAttack = abilityEls[1].querySelector("ul > li").dataset.value;
        const abilityDefense = abilityEls[2].querySelector("ul > li").dataset.value;
        const abilitySpecialAttack = abilityEls[3].querySelector("ul > li").dataset.value;
        const abilitySpecialDefense = abilityEls[4].querySelector("ul > li").dataset.value;
        const abilitySpeed = abilityEls[5].querySelector("ul > li").dataset.value;
        const nextUrl = doc.querySelector(".next").getAttribute("href");

        // Attributes
        const attributesElsContainers = doc.querySelectorAll(".pokemon-ability-info > div");
        const attributesElsLeft = attributesElsContainers[0].querySelectorAll("ul > li");
        const attributeHeight = attributesElsLeft[0]
          .querySelector(".attribute-value")
          .innerHTML.replace(/[a-z]/g, "")
          .trim();
        const attributeWeight = attributesElsLeft[1]
          .querySelector(".attribute-value")
          .innerHTML.replace(/[a-z]/g, "")
          .trim();
        const attributeGenderMale = attributesElsLeft[2].querySelector(".icon_male_symbol") ? true : false;
        const attributeGenderFemale = attributesElsLeft[2].querySelector(".icon_female_symbol") ? true : false;
        const attributesElsRight = attributesElsContainers[1].querySelectorAll("ul > li");
        const attributeCategory = attributesElsRight[0].querySelector(".attribute-value").innerHTML;
        const attributeList = Array.from(
          attributesElsRight[1].querySelector("ul").querySelectorAll(".attribute-value")
        ).map((entry) => entry.innerHTML);

        // Attack and weaknesses
        const attackList = Array.from(
          doc.querySelectorAll(".pokedex-pokemon-attributes > div:nth-child(1) > ul > li")
        ).map((entry) => entry.querySelector("a").innerHTML);
        const weaknessesList = Array.from(
          doc.querySelectorAll(".pokedex-pokemon-attributes > div:nth-child(2) > ul > li")
        ).map((entry) =>
          entry
            .querySelector("a > span")
            .innerHTML.replace(/\<i.*(\/)?/g, "")
            .replace(/\\n\\t/g, "")
            .trim()
        );

        // Apply
        const data = {
          name: pokemonName,
          number: pokemonNumber,
          url,
          attributes: {
            hp: abilityHP,
            attack: abilityAttack,
            defense: abilityDefense,
            specialAttack: abilitySpecialAttack,
            specialDefense: abilitySpecialDefense,
            speed: abilitySpeed,
          },
          abilities: {
            height: attributeHeight,
            weight: attributeWeight,
            male: attributeGenderMale,
            female: attributeGenderFemale,
            category: attributeCategory,
            abilities: attributeList,
            attackList,
            weaknessesList,
          },
        };

        console.log(data);
        outputData.push(data);

        // // Call next page
        if (!loadedPokemonsUrls.includes(nextUrl)) {
          fs.writeFile("pokemonData.json", JSON.stringify(outputData), function (err) {
            if (err) {
              console.log(err);
              process.exit();
            }
          });
          page.close();
          fetchPage(nextUrl);
        } else {
          // Save file
          // End program
          console.log("DONE");
          process.exit();
        }
      });
  });
};

const getNextPage = (url) => {
  puppeteer
    .launch()
    .then((browser) => {
      return browser.newPage();
    })
    .then((page) => {
      return page
        .goto(`https://www.pokemon.com${url}`)
        .then(() => {
          return page.content();
        })
        .then((response) => {
          const dom = new JSDOM(response);
          const doc = dom.window.document;

          const nextUrl = doc.querySelector(".next").getAttribute("href");

          fetchPage(nextUrl);
        });
    });
};

const launchPuppeteer = () => {
  puppeteer.launch().then((browser) => {
    puppeteerBrowser = browser;
    if (loadedPokemonsUrls.length) {
      getNextPage(loadedPokemonsUrls[loadedPokemonsUrls.length - 1]);
    } else {
      fetchPage("/br/pokedex/bulbasaur");
    }
  });
};

// Start
fs.readFile("pokemonData.json", "utf8", (err, fileData) => {
  if (err) {
    outputData = [];
    loadedPokemonsUrls = [];
  } else {
    outputData = JSON.parse(fileData);
    loadedPokemonsUrls = outputData.map((entry) => entry.url);
  }
  launchPuppeteer();
});
